.PHONY: asset

OUTPUT_NAME = talk
MAIN_NAME = talk

all: text

text: $(OUTPUT_NAME).pdf

$(OUTPUT_NAME).pdf: $(MAIN_NAME).tex
	latexmk -pdf -halt-on-error $(MAIN_NAME).tex -jobname=$(OUTPUT_NAME) -shell-escape


